#! /bin/bash
set -e -o pipefail

DB="$1"
ARGS="${@:2}"

#trap "curl --max-time 30 -s -f -XPOST http://127.0.0.1:15020/quitquitquit" EXIT
#while ! curl -s -f http://127.0.0.1:15020/healthz/ready; do sleep 1; done
#echo "Ready!"

sleep 20

mc alias set pg "$MINIO_SERVER" "$MINIO_ACCESS_KEY" "$MINIO_SECRET_KEY" --api "$MINIO_API_VERSION" > /dev/null

ARCHIVE="${MINIO_BUCKET}/${DB}-$(date $DATE_FORMAT).archive"

echo "Dumping $DB to $ARCHIVE"
echo "> pg_dump ${ARGS} -Fc $DB"

pg_dump $ARGS -Fc "$DB" | mc pipe "pg/$ARCHIVE" || { echo "Backup failed"; mc rm "pg/$ARCHIVE"; exit 1; }

echo "Backup complete"


curl --max-time 30 -s -f -XPOST http://127.0.0.1:15020/quitquitquit
